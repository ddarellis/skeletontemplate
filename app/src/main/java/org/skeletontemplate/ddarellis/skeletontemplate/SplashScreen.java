package org.skeletontemplate.ddarellis.skeletontemplate;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;

import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.ErrorCodes;
import com.firebase.ui.auth.IdpResponse;
import com.firebase.ui.auth.ResultCodes;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GetTokenResult;
import com.google.firebase.auth.UserInfo;
import com.google.firebase.database.FirebaseDatabase;

import org.skeletontemplate.ddarellis.skeletontemplate.entity.User;
import org.skeletontemplate.ddarellis.skeletontemplate.view.activity.MainActivity;

import java.util.Arrays;

public class SplashScreen extends AppCompatActivity {

    private static final int RC_SIGN_IN = 9000;

    private static final String ARG_ID_TOKEN = "ARG_ID_TOKEN";

    /**
     * Duration of wait
     **/
    private final int SPLASH_DISPLAY_LENGTH = 2000;
    Runnable signInRunnable;
    private Handler handler;
    private String idToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        handler = new Handler();
        signInRunnable = new Runnable() {
            @Override
            public void run() {
                signIn();
            }
        };
        handler.postDelayed(signInRunnable, SPLASH_DISPLAY_LENGTH);

    }

    private void signIn() {
        FirebaseAuth auth = FirebaseAuth.getInstance();
        if (auth.getCurrentUser() != null) {
            FirebaseUser mUser = FirebaseAuth.getInstance().getCurrentUser();
            mUser.getToken(true)
                    .addOnCompleteListener(new OnCompleteListener<GetTokenResult>() {
                        public void onComplete(@NonNull Task<GetTokenResult> task) {
                            if (task.isSuccessful()) {
                                idToken = task.getResult().getToken();
                            }
                        }
                    });
            startActivity(new Intent(SplashScreen.this, MainActivity.class)
                    .putExtra(ARG_ID_TOKEN, mUser.getUid()));
            finish();
            return;
        } else {
            startActivityForResult(
                    AuthUI.getInstance()
                            .createSignInIntentBuilder()
                            .setIsSmartLockEnabled(!BuildConfig.DEBUG)
                            .setProviders(Arrays.asList(new AuthUI.IdpConfig.Builder(AuthUI.EMAIL_PROVIDER).build(),
                                    new AuthUI.IdpConfig.Builder(AuthUI.GOOGLE_PROVIDER).build(),
                                    new AuthUI.IdpConfig.Builder(AuthUI.FACEBOOK_PROVIDER).build(),
                                    new AuthUI.IdpConfig.Builder(AuthUI.TWITTER_PROVIDER).build()))
                            .build(),
                    RC_SIGN_IN);
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // RC_SIGN_IN is the request code you passed into startActivityForResult(...) when starting the sign in flow.
        if (requestCode == RC_SIGN_IN) {
            IdpResponse response = IdpResponse.fromResultIntent(data);
            // Successfully signed in
            if (resultCode == ResultCodes.OK) {
                //IdpResponse idpResponse = IdpResponse.fromResultIntent(data);
                addUserToBatabase();
                FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
                startActivity(new Intent(this, MainActivity.class)
                        .putExtra(ARG_ID_TOKEN, currentUser.getUid()));
                handler.removeCallbacks(signInRunnable);
                finish();
                return;
            } else {
                handler.postDelayed(signInRunnable, SPLASH_DISPLAY_LENGTH);
                // Sign in failed
                if (response == null) {
                    // User pressed back button
                    Snackbar.make(findViewById(R.id.content_main), R.string.sign_in_cancelled, Snackbar.LENGTH_LONG).show();
                    return;
                }

                if (response.getErrorCode() == ErrorCodes.NO_NETWORK) {
                    Snackbar.make(findViewById(R.id.content_main), R.string.no_internet_connection, Snackbar.LENGTH_LONG).show();
                    return;
                }

                if (response.getErrorCode() == ErrorCodes.UNKNOWN_ERROR) {
                    Snackbar.make(findViewById(R.id.content_main), R.string.unknown_error, Snackbar.LENGTH_LONG).show();
                    return;
                }
            }
            Snackbar.make(findViewById(R.id.content_main), R.string.unknown_sign_in_response, Snackbar.LENGTH_LONG).show();
        }
    }

    private void addUserToBatabase() {

        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        User user = new User(currentUser.getUid(), currentUser.getDisplayName(), currentUser.getEmail(), currentUser.getPhotoUrl().toString());
        for (UserInfo profile : currentUser.getProviderData()) {
            // check if the provider id matches "facebook.com"
            if (profile.getProviderId().equals(getString(R.string.facebook_provider_id))) {
                user.setPhoto("https://graph.facebook.com/" + profile.getUid() + "/picture?height=600");
            }
        }
        FirebaseDatabase.getInstance().getReference().child("users").child(currentUser.getUid()).setValue(user);

    }

    @Override
    public void onDestroy() {
        handler.removeCallbacks(signInRunnable);
        super.onDestroy();
    }

}