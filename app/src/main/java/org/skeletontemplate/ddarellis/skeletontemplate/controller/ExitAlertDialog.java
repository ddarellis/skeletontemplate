package org.skeletontemplate.ddarellis.skeletontemplate.controller;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import org.skeletontemplate.ddarellis.skeletontemplate.R;

/**
 * Created by ddarellis on 3/22/2017.
 */

public class ExitAlertDialog {

    public static void showDialog(final Context context) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        ((Activity) context).finish();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(context.getString(R.string.exit_message)).setPositiveButton(context.getString(R.string.yes), dialogClickListener)
                .setNegativeButton(R.string.no, dialogClickListener).show();

    }
}
