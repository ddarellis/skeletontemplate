package org.skeletontemplate.ddarellis.skeletontemplate.controller;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Build;
import android.preference.PreferenceManager;

import org.skeletontemplate.ddarellis.skeletontemplate.R;

import java.util.Locale;

/**
 * Created by ddarellis on 3/24/2017.
 */

public class LocaleController {

    public void updateLocale(Context context, Configuration config, Locale currentLocale) {
        Locale.setDefault(currentLocale);
        config = new Configuration(config);
        config.locale = currentLocale;
        context.getResources().updateConfiguration(config,
                context.getResources().getDisplayMetrics());
    }

    public void updateLocale(Context context, Locale locale) {
        Locale.setDefault(locale);
        Configuration config = context.getResources().getConfiguration();
        config = new Configuration(config);
        config.locale = locale;
        context.getResources().updateConfiguration(config,
                context.getResources().getDisplayMetrics());

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(context.getString(R.string.pref_locale), locale.getLanguage());
        editor.apply();
    }

    public Locale getCurrentLocale(Context context) {
        Locale currentLocale;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            currentLocale = context.getResources().getConfiguration().getLocales().get(0);
        } else {
            currentLocale = context.getResources().getConfiguration().locale;
        }

        return currentLocale;
    }
}
