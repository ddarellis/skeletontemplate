package org.skeletontemplate.ddarellis.skeletontemplate.controller;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import org.skeletontemplate.ddarellis.skeletontemplate.view.activity.IntroActivity;
import org.skeletontemplate.ddarellis.skeletontemplate.view.activity.MainActivity;

/**
 * Created by ddarellis on 3/22/2017.
 */

public class IntroController {

    /**
     * Declare a new thread to do a preference check
     */
    public static void showIntro(final MainActivity mainActivity) {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                //  Initialize SharedPreferences
                SharedPreferences getPrefs = PreferenceManager
                        .getDefaultSharedPreferences(mainActivity);

                //  Create a new boolean and preference and set it to true
                boolean isFirstStart = getPrefs.getBoolean("firstStart", true);
                //isFirstStart = true;//TODO!!!!
                //  If the activity has never started before...
                if (isFirstStart) {
                    //  Launch app intro
                    Intent intent = new Intent(mainActivity.getBaseContext(), IntroActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mainActivity.startActivity(intent);

                    //  Make a new preferences editor
                    SharedPreferences.Editor e = getPrefs.edit();

                    //  Edit preference to make it false because we don't want this to run again
                    e.putBoolean("firstStart", false);

                    //  Apply changes
                    e.apply();
                }
            }
        });

        // Start the thread
        t.start();
    }
}
