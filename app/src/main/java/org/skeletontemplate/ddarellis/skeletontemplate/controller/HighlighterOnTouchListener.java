package org.skeletontemplate.ddarellis.skeletontemplate.controller;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

import org.skeletontemplate.ddarellis.skeletontemplate.R;
import org.skeletontemplate.ddarellis.skeletontemplate.view.activity.MainActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by ddarellis on 3/22/2017.
 */

public class HighlighterOnTouchListener implements View.OnTouchListener {

    private static final int TRANSPARENT_GREY = Color.argb(0, 185, 185, 185);
    private static final int FILTERED_GREY = Color.argb(155, 185, 185, 185);

    private final ImageView imageView;
    private final Context context;
    private List<ImageView> images = new ArrayList<>();

    public HighlighterOnTouchListener(Context context, final ImageView imageView, List<ImageView> images) {
        super();
        this.imageView = imageView;
        this.images = images;
        this.context = context;
    }

    public boolean onTouch(final View view, final MotionEvent motionEvent) {
        if (imageView != null) {
            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                imageView.setColorFilter(FILTERED_GREY);
            } else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                imageView.setColorFilter(TRANSPARENT_GREY); // or null
            }
        }

        for (ImageView image : images) {
            image.setBackground(null);
        }

        imageView.setBackground(ContextCompat.getDrawable(view.getContext(), R.drawable.selected_border));
        ((MainActivity) context).updateLocale(new Locale(imageView.getTag().toString()));


        return true;
    }

}