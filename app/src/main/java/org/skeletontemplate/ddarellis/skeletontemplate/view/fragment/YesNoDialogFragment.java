package org.skeletontemplate.ddarellis.skeletontemplate.view.fragment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

/**
 * Created by ddarellis on 3/22/2017.
 */

public class YesNoDialogFragment extends DialogFragment {

    private static final String TITLE = "org.skeletontemplate.ddarellis.skeletontemplate.view.fragment.Title";
    private static final String MESSAGE = "org.skeletontemplate.ddarellis.skeletontemplate.view.fragment.message";

    /**
     * @return The new fragment.
     */
    public static YesNoDialogFragment newInstance(String title, String message) {
        Bundle args = new Bundle();
        args.putString(TITLE, title);
        args.putString(MESSAGE, message);

        YesNoDialogFragment newFragment = new YesNoDialogFragment();
        newFragment.setArguments(args);

        return newFragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        ProgressDialog reconnectDialog = new ProgressDialog(getActivity());
        reconnectDialog.setTitle(getArguments().getString(TITLE));
        reconnectDialog.setMessage(getArguments().getString(MESSAGE));
        reconnectDialog.setCancelable(false);
        reconnectDialog.setCanceledOnTouchOutside(false);
        reconnectDialog.setIndeterminate(true);
        reconnectDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "tost", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        reconnectDialog.setButton(DialogInterface.BUTTON_POSITIVE, "tost", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                getActivity().finish();
            }
        });

        return reconnectDialog;
    }

}