package org.skeletontemplate.ddarellis.skeletontemplate.view.activity;

import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;

import com.github.paolorotolo.appintro.AppIntro;
import com.github.paolorotolo.appintro.AppIntroFragment;

import org.skeletontemplate.ddarellis.skeletontemplate.R;
import org.skeletontemplate.ddarellis.skeletontemplate.view.fragment.intro.IntroFragmentOne;
import org.skeletontemplate.ddarellis.skeletontemplate.view.fragment.intro.IntroFragmentTwo;

public class IntroActivity extends AppIntro implements IntroFragmentOne.OnFragmentInteractionListener, IntroFragmentTwo.OnFragmentInteractionListener {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Note here that we DO NOT use setContentView();

        // Add your slide fragments here.
        // AppIntro will automatically generate the dots indicator and buttons.
        //addSlide(new IntroFragmentOne());
        //addSlide(new IntroFragmentTwo());

        // Instead of fragments, you can also use our default slide
        // Just set a title, description, background and image. AppIntro will do the rest.
        addSlide(AppIntroFragment.newInstance("title", "description", R.drawable.grinning_face, ContextCompat.getColor(this, R.color.introBlue)));
        addSlide(AppIntroFragment.newInstance("title", "description", R.drawable.ic_navigate_before_white, ContextCompat.getColor(this, R.color.introGreen)));
        addSlide(AppIntroFragment.newInstance("title", "description", R.drawable.ic_navigate_before_white, ContextCompat.getColor(this, R.color.introYellow)));
        addSlide(AppIntroFragment.newInstance("title", "description", R.drawable.ic_navigate_before_white, ContextCompat.getColor(this, R.color.colorPrimary)));

        // OPTIONAL METHODS
        // Override bar/separator color.
        //setBarColor(Color.parseColor("#2196F3"));
        setSeparatorColor(Color.parseColor("#CCCC00"));

        // Hide Skip/Done button.
        //showSkipButton(false);
        //setProgressButtonEnabled(false);

        // Turn vibration on and set intensity.
        // NOTE: you will probably need to ask VIBRATE permission in Manifest.
        //setVibrate(true);
        //setVibrateIntensity(30);
    }

    @Override
    public void onSkipPressed(Fragment currentFragment) {
        super.onSkipPressed(currentFragment);
        // Do something when users tap on Skip button.
        finish();
    }

    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);
        // Do something when users tap on Done button.
        finish();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}