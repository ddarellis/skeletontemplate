package org.skeletontemplate.ddarellis.skeletontemplate.controller;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.skeletontemplate.ddarellis.skeletontemplate.R;

import java.util.List;

/**
 * Created by ddarellis on 3/23/2017.
 */

public class ListArrayAdapter extends ArrayAdapter<String> {
    private final List<String> values;

    public ListArrayAdapter(Context context, int layout, List<String> values) {
        super(context, layout, values);
        this.values = values;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        if (null == convertView) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_item, parent, false);
        }

        TextView textView = (TextView) convertView.findViewById(R.id.title_text);
        ImageView imageView = (ImageView) convertView.findViewById(R.id.list_image);
        textView.setText(values.get(position));

        imageView.setImageResource(R.drawable.common_full_open_on_phone);

        return convertView;
    }

}