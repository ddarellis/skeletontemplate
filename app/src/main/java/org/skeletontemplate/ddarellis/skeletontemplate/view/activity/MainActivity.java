package org.skeletontemplate.ddarellis.skeletontemplate.view.activity;

import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.wang.avi.AVLoadingIndicatorView;

import org.skeletontemplate.ddarellis.skeletontemplate.R;
import org.skeletontemplate.ddarellis.skeletontemplate.controller.ExitAlertDialog;
import org.skeletontemplate.ddarellis.skeletontemplate.controller.IntroController;
import org.skeletontemplate.ddarellis.skeletontemplate.controller.LocaleController;
import org.skeletontemplate.ddarellis.skeletontemplate.view.fragment.ListFragmentExample;
import org.skeletontemplate.ddarellis.skeletontemplate.view.fragment.ProfileFragment;
import org.skeletontemplate.ddarellis.skeletontemplate.view.fragment.SettingsFragment;

import java.util.Locale;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, ListFragmentExample.OnFragmentInteractionListener, SettingsFragment.OnFragmentInteractionListener, ProfileFragment.OnFragmentInteractionListener {

    private static final String ARG_ID_TOKEN = "ARG_ID_TOKEN";
    private GoogleMapActivity googleMapActivity;
    private Locale locale = null;
    private LocaleController localeController;
    private String idToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                idToken = null;
            } else {
                idToken = extras.getString(ARG_ID_TOKEN);
            }
        } else {
            idToken = (String) savedInstanceState.getSerializable(ARG_ID_TOKEN);
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        IntroController.showIntro(this);

        localeController = new LocaleController();

        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);

        String lang = settings.getString(getString(R.string.pref_locale), "");
        Locale currentLocale = localeController.getCurrentLocale(this);
        if (!"".equals(lang) && !currentLocale.getLanguage().equals(lang)) {
            locale = new Locale(lang);
            updateLocale(locale);
        }

    }

    /**
     * Forwards the request for permissions to google maps fragment.
     *
     * @param requestCode  The request code passed.
     * @param permissions  The permissions.
     * @param grantResults The grant results for the corresponding permissions.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        if (requestCode == GoogleMapActivity.MY_PERMISSIONS_REQUEST_LOCATION) {
            googleMapActivity.onRequestPermissionsResult(requestCode, permissions, grantResults);
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    /**
     * On configuration chenge update the locale.
     *
     * @param newConfig New config.
     */
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (locale != null) {
            localeController.updateLocale(getBaseContext(), newConfig, locale);
        }
    }

    /**
     * Updates locale and UI resources.
     *
     * @param locale The new locale.
     */
    public void updateLocale(Locale locale) {
        localeController.updateLocale(getBaseContext(), locale);
        this.locale = locale;
        updateUIResources();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Updates the UI resources for drawer.
     * This needs to be done cause when updating locale the drawer does not change locale.
     * This is cause we have to restart main activity or get the resources like this again.
     */
    private void updateUIResources() {
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);

        Menu menu = navigationView.getMenu();

        MenuItem nav_menu = menu.findItem(R.id.nav_map);
        nav_menu.setTitle(R.string.nav_map);

        nav_menu = menu.findItem(R.id.nav_list);
        nav_menu.setTitle(R.string.nav_list);

        nav_menu = menu.findItem(R.id.nav_gallery);
        nav_menu.setTitle(R.string.nav_gallery);

        nav_menu = menu.findItem(R.id.nav_slideshow);
        nav_menu.setTitle(R.string.nav_slideshow);

        nav_menu = menu.findItem(R.id.nav_settings);
        nav_menu.setTitle(R.string.nav_settings);

        nav_menu = menu.findItem(R.id.nav_title_communicate);
        nav_menu.setTitle(R.string.nav_title_communicate);

        nav_menu = menu.findItem(R.id.nav_share);
        nav_menu.setTitle(R.string.nav_share);

        nav_menu = menu.findItem(R.id.nav_send);
        nav_menu.setTitle(R.string.nav_send);

    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Fragment fragment = null;
        FragmentManager fragmentManager = getSupportFragmentManager(); // For AppCompat use getSupportFragmentManager

        if (id == R.id.nav_map) {
            googleMapActivity = new GoogleMapActivity();
            fragment = googleMapActivity;
        } else if (id == R.id.nav_list) {
            fragment = ListFragmentExample.newInstance(idToken);
        } else if (id == R.id.nav_gallery) {
            AVLoadingIndicatorView avi = (AVLoadingIndicatorView) findViewById(R.id.avi);
            avi.show();
        } else if (id == R.id.nav_slideshow) {
            AVLoadingIndicatorView avi = (AVLoadingIndicatorView) findViewById(R.id.avi);
            avi.hide();
        } else if (id == R.id.nav_settings) {
            fragment = SettingsFragment.newInstance();
        } else if (id == R.id.nav_share) {
            fragment = ListFragmentExample.newInstance(idToken);
        } else if (id == R.id.nav_send) {
            fragment = ProfileFragment.newInstance(idToken);
        }
        if (null != fragment) {
            fragmentManager.beginTransaction()
                    .replace(R.id.content_main, fragment)
                    .commit();
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
        } else {
            ExitAlertDialog.showDialog(this);
        }

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
