package org.skeletontemplate.ddarellis.skeletontemplate.view.fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import org.skeletontemplate.ddarellis.skeletontemplate.R;
import org.skeletontemplate.ddarellis.skeletontemplate.SplashScreen;
import org.skeletontemplate.ddarellis.skeletontemplate.controller.HighlighterOnTouchListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SettingsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SettingsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SettingsFragment extends Fragment implements View.OnClickListener {

    private final List<ImageView> flagImages = new ArrayList<>();

    private OnFragmentInteractionListener mListener;

    public SettingsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment SettingsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SettingsFragment newInstance() {
        return new SettingsFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings, container, false);

        ImageView greekFlagImage = (ImageView) view.findViewById(R.id.greekFlagImageView);
        ImageView englishFlagImage = (ImageView) view.findViewById(R.id.englishFlagImageView);

        Button signOutButton = (Button) view.findViewById(R.id.sign_out);
        signOutButton.setOnClickListener(this);
        Button deleteAccountButton = (Button) view.findViewById(R.id.delete_account);
        deleteAccountButton.setOnClickListener(this);

        flagImages.add(greekFlagImage);
        flagImages.add(englishFlagImage);

        greekFlagImage.setOnTouchListener(new HighlighterOnTouchListener(getActivity(), greekFlagImage, flagImages));
        englishFlagImage.setOnTouchListener(new HighlighterOnTouchListener(getActivity(), englishFlagImage, flagImages));

        initilizeFlags(view);

        return view;
    }


    private void initilizeFlags(View view) {
        for (ImageView image : flagImages) {

            System.out.println(Locale.getDefault().getLanguage() + " !!!!!! " + image.getTag());

            if (Locale.getDefault().getLanguage().equals(image.getTag())) {

                image.setBackground(ContextCompat.getDrawable(view.getContext(), R.drawable.selected_border));
                break;
            }
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.sign_out) {
            AuthUI.getInstance()
                    .signOut(getActivity())
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        public void onComplete(@NonNull Task<Void> task) {
                            // user is now signed out
                            startActivity(new Intent(getActivity(), SplashScreen.class));
                            getActivity().finish();
                        }
                    });
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
